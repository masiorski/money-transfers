package com.revolut.moneytransfers.models;

import com.revolut.moneytransfers.interfaces.Currency;
import com.revolut.moneytransfers.models.currencies.BaseCurrency;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BaseCurrencyTest {

    @Test
    void valueOf() {
        final Currency pln = BaseCurrency.valueOf("PLN");
        assertNotNull(pln);
        assertTrue(pln.isValid());
        assertFalse(pln.isNotValid());
        assertEquals("PLN", pln.getISOCode());
        assertEquals(BaseCurrency.valueOf("PLN"), pln);
        assertSame(BaseCurrency.valueOf("PLN"), pln);

        final Currency usd = BaseCurrency.valueOf("USD");
        assertNotNull(usd);
        assertTrue(usd.isValid());
        assertFalse(usd.isNotValid());
        assertEquals("USD", usd.getISOCode());
        assertEquals(BaseCurrency.valueOf("USD"), usd);
        assertSame(BaseCurrency.valueOf("USD"), usd);

        assertNotEquals(usd, pln);
    }

    @Test
    void defaultCurrency() {
        final Currency def = BaseCurrency.getDefault();
        assertNotNull(def);
        assertTrue(def.isValid());
        assertEquals("USD", def.getISOCode());
    }
}