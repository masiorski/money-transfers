package com.revolut.moneytransfers.models;

import com.revolut.moneytransfers.enums.HolderType;
import com.revolut.moneytransfers.interfaces.Holder;
import com.revolut.moneytransfers.models.holders.AbstractHolder;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

class PrivatePersonTest {

    @Test
    void attributes() {
        final Holder holder = AbstractHolder.makePrivatePerson(15L, "123456789012", "Jon", "Snow");
        assertNotNull(holder);
        assertEquals(Long.valueOf(15L), holder.getId());
        assertTrue(holder.isValid());
        assertFalse(holder.isNotValid());
        assertFalse(holder.isLegalPerson());
        assertTrue(holder.isPrivatePerson());
        assertEquals(HolderType.PRIVATE_PERSON, holder.getType());
        assertEquals("Jon Snow", holder.getName());
        assertEquals("123456789012", holder.getTaxNumber());
    }
}