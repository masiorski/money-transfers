package com.revolut.moneytransfers.models;

import com.revolut.moneytransfers.enums.HolderType;
import com.revolut.moneytransfers.interfaces.Holder;
import com.revolut.moneytransfers.models.holders.AbstractHolder;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

class LegalPersonTest {

    @Test
    void attributes() {
        final Holder holder = AbstractHolder.makeLegalPerson(11L, "1234567890", "Stark");
        assertNotNull(holder);
        assertEquals(Long.valueOf(11L), holder.getId());
        assertTrue(holder.isValid());
        assertFalse(holder.isNotValid());
        assertTrue(holder.isLegalPerson());
        assertFalse(holder.isPrivatePerson());
        assertEquals(HolderType.LEGAL_PERSON, holder.getType());
        assertEquals("Stark", holder.getName());
        assertEquals("1234567890", holder.getTaxNumber());
    }
}