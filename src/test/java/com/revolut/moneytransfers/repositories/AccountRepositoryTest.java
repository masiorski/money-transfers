package com.revolut.moneytransfers.repositories;

import com.revolut.moneytransfers.interfaces.Account;
import com.revolut.moneytransfers.interfaces.Holder;
import com.revolut.moneytransfers.interfaces.repositories.AccountRepository;
import com.revolut.moneytransfers.interfaces.repositories.HolderRepository;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

class AccountRepositoryTest {

    @Test
    void getInitialBalance() {
        final AccountRepository repository = ApplicationContext.initialize().getAccountRepository();
        assertEquals(BigDecimal.valueOf(100_000_000.00d), repository.getInitialBalance());
    }

    @Test
    void getOurBankMainAccount() {
        final AccountRepository repository = ApplicationContext.initialize().getAccountRepository();
        final Account a = repository.getOurBankMainAccount();
        assertNotNull(a);
        assertTrue(a.isValid());
        assertTrue(a.isActive());
        assertEquals(repository.getInitialBalance(), a.getBalance());
    }

    @Test
    void size() {
        final AccountRepository repository = ApplicationContext.initialize().getAccountRepository();
        assertEquals(1, repository.size());
        repository.addOurBankAccount("20202810100000012345", BigDecimal.ZERO);
        assertEquals(2, repository.size());
    }

    @Test
    void getByHolder() {
        final ApplicationContext applicationContext = ApplicationContext.initialize();
        final AccountRepository repository = applicationContext.getAccountRepository();
        final HolderRepository holderRepository = applicationContext.getHolderRepository();
        final Holder holder = holderRepository.addLegalPerson("1234567890", "test");

        Collection<Account> accounts = repository.getByHolder(holder);
        assertNotNull(accounts);
        assertEquals(0, accounts.size());

        final Account clientAccount = repository.addPassiveAccount("40702810001234567890", holder);
        accounts = repository.getByHolder(holder);
        assertEquals(1, accounts.size());
        assertEquals(clientAccount, accounts.iterator().next());

        accounts = repository.getByHolder(holderRepository.getOurBank());
        assertEquals(1, accounts.size());
        assertEquals(repository.getOurBankMainAccount(), accounts.iterator().next());
    }
}