package com.revolut.moneytransfers.repositories;

import com.revolut.moneytransfers.interfaces.Holder;
import com.revolut.moneytransfers.interfaces.repositories.HolderRepository;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

class HolderRepositoryTest {

    @Test
    void getById() {
        final HolderRepository repository = make();
        Holder pt = repository.getById(1L);
        assertNotNull(pt);
        assertTrue(pt.isValid());

        pt = repository.getById(Long.MAX_VALUE);
        assertNotNull(pt);
        assertFalse(pt.isValid());
        assertEquals(repository.getInvalid(), pt);
    }

    @Test
    void getOurBank() {
        final HolderRepository repository = make();
        final Holder pt = repository.getOurBank();
        assertNotNull(pt);
        assertTrue(pt.isValid());
        assertTrue(pt.isLegalPerson());
        assertEquals(Long.valueOf(1), pt.getId());
        assertEquals("7703408188", pt.getTaxNumber());
        assertEquals("Revolut LLC", pt.getName());
    }

    private HolderRepository make() {
        return new DefaultHolderRepository();
    }
}