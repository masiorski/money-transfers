package com.revolut.moneytransfers.interfaces;

import com.revolut.moneytransfers.enums.HolderType;

public interface Holder extends Identifiable {

    String getName();

    boolean isPrivatePerson();

    boolean isLegalPerson();

    HolderType getType();

    String getTaxNumber();
}
