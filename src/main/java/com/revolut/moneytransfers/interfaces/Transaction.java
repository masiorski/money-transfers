package com.revolut.moneytransfers.interfaces;

import com.revolut.moneytransfers.enums.TransactionState;

import java.math.BigDecimal;

public interface Transaction extends Identifiable {

    Account getFrom();

    Account getTo();

    BigDecimal getAmount();

    TransactionState getState();

    boolean run();
}
