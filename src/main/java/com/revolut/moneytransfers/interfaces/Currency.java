package com.revolut.moneytransfers.interfaces;

public interface Currency extends Validatable {

    int ISO_CODE_LENGTH = 3;

    String getISOCode();
}
