package com.revolut.moneytransfers.interfaces.repositories;

import com.revolut.moneytransfers.interfaces.Validatable;

public interface Pagination extends Validatable {

    int getRecordsPerPage();

    int getPageNumber();
}
