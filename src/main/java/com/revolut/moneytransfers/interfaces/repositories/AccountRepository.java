package com.revolut.moneytransfers.interfaces.repositories;

import com.revolut.moneytransfers.interfaces.Account;
import com.revolut.moneytransfers.interfaces.Currency;
import com.revolut.moneytransfers.interfaces.Holder;

import java.math.BigDecimal;
import java.util.Collection;

public interface AccountRepository extends Repository<Account> {

    Account addOurBankAccount(Currency currency, String number, BigDecimal balance);

    Account addOurBankAccount(String number, BigDecimal balance);

    Account getOurBankMainAccount();

    Account addPassiveAccount(Currency currency, String number, Holder holder);

    Account addPassiveAccount(String number, Holder holder);

    BigDecimal getInitialBalance();

    void validateBalance();

    Collection<Account> getByHolder(Holder holder);
}
