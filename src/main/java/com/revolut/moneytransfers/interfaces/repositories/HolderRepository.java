package com.revolut.moneytransfers.interfaces.repositories;

import com.revolut.moneytransfers.interfaces.Holder;

public interface HolderRepository extends Repository<Holder> {

    Holder addLegalPerson(String taxIdentificationNumber, String name);

    Holder addPrivatePerson(String taxIdentificationNumber, String firstName, String lastName);

    Holder getOurBank();
}
