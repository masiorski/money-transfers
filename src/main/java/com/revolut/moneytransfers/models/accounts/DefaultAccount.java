package com.revolut.moneytransfers.models.accounts;

import com.revolut.moneytransfers.interfaces.Currency;
import com.revolut.moneytransfers.interfaces.Holder;

import java.math.BigDecimal;

public final class DefaultAccount extends AbstractAccount {

    private DefaultAccount(Long id, Currency currency, String number, Holder holder, boolean active,
                           BigDecimal balance) {
        super(id, currency, number, holder, active, balance);
        validateNumber(number);
    }

    private DefaultAccount(Long id, Currency currency, String number, Holder holder, boolean active) {
        this(id, currency, number, holder, active, BigDecimal.ZERO);
    }

    @Override
    public String toString() {
        final String base = super.toString();
        return base.replace("Account{", "DefaultAccount{").replaceFirst("(?s)(.*)}", "$1");
    }

    private static void validateNumber(String number) {
        if (number.length() != 20) {
            throw new IllegalArgumentException("AbstractAccount number must contain 20 characters");
        }
    }

    public static DefaultAccount makeActiveBalance(Long id, Currency currency, String number, Holder holder, BigDecimal balance) {
        return new DefaultAccount(id, currency, number, holder, true, balance);
    }

    public static DefaultAccount makeActiveBalance(Long id, Currency currency, String number, Holder holder) {
        return new DefaultAccount(id, currency, number, holder, true);
    }

    public static DefaultAccount makePassiveBalance(Long id, Currency currency, String number, Holder holder) {
        return new DefaultAccount(id, currency, number, holder, false);
    }
}
