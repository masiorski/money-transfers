package com.revolut.moneytransfers.models.transactions;

import com.revolut.moneytransfers.enums.TransactionState;
import com.revolut.moneytransfers.interfaces.Account;
import com.revolut.moneytransfers.interfaces.Transaction;
import com.revolut.moneytransfers.utils.validators.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;

import static com.revolut.moneytransfers.Constants.LOCK_WAIT_INTERVAL;

public class MoneyTransaction implements Transaction {

    private static final  Logger LOGGER = LoggerFactory.getLogger(MoneyTransaction.class);

    private final Long id;
    private final Account from;
    private final Account to;
    private final BigDecimal amount;
    private TransactionState state;

    MoneyTransaction(Long id, Account from, Account to, BigDecimal amount) {
        Objects.requireNonNull(id, "Id cannot be null");
        Objects.requireNonNull(from, "Origin account cannot be null");
        Objects.requireNonNull(to, "Destination account cannot be null");
        Objects.requireNonNull(amount, "Amount cannot be null");
        Validator.validateAmountPositive(amount);
        Validator.validateAccountsAreValid(from, to);
        Validator.validateAccountIsDifferent(from, to);
        Validator.validateCurrencyIsTheSame(from, to);

        this.id = id;
        this.from = from;
        this.to = to;
        this.amount = amount;
        this.state = TransactionState.NEW;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public Account getFrom() {
        return from;
    }

    @Override
    public Account getTo() {
        return to;
    }

    @Override
    public BigDecimal getAmount() {
        return amount;
    }

    @Override
    public synchronized TransactionState getState() {
        return state;
    }

    @Override
    public synchronized boolean run() {
        if (state != TransactionState.COMPLETED) {
            changeState();
            return doRun();
        }
        return false;
    }

    private boolean doRun() {
        final Lock fromLock = from.writeLock();
        try {
            if (fromLock.tryLock(LOCK_WAIT_INTERVAL, TimeUnit.MILLISECONDS)) {
                try {
                    final Lock toLock = to.writeLock();
                    if (toLock.tryLock(LOCK_WAIT_INTERVAL, TimeUnit.MILLISECONDS)) {
                        try {
                            if (from.debit(amount)) {
                                if (to.credit(amount)) {
                                    state = TransactionState.COMPLETED;
                                     LOGGER.trace("Transaction {} completed", id);
                                    return true;
                                }
                            }
                            state = TransactionState.INSUFFICIENT_FUNDS;
                        } finally {
                            toLock.unlock();
                        }
                    } else {
                        state = TransactionState.CONCURRENCY_ERROR;
                    }
                } finally {
                    fromLock.unlock();
                }
            } else {
                state = TransactionState.CONCURRENCY_ERROR;
            }
        } catch (InterruptedException e) {
            state = TransactionState.CONCURRENCY_ERROR;
             LOGGER.error(e.getLocalizedMessage(), e);
        }
        return false;
    }

    private void changeState() {
        switch (state) {
            case INSUFFICIENT_FUNDS:
            case CONCURRENCY_ERROR:
                state = TransactionState.RESTARTED;
                break;
        }
    }

    public static Transaction getInvalid() {
        return InvalidTransaction.getInstance();
    }

    public static Transaction make(Long id, Account debit, Account credit, BigDecimal amount) {
        return new MoneyTransaction(id, debit, credit, amount);
    }
}
