package com.revolut.moneytransfers.models.holders;

import com.revolut.moneytransfers.enums.HolderType;
import com.revolut.moneytransfers.interfaces.Identifiable;

final public class InvalidHolder extends AbstractHolder {

    private InvalidHolder() {
        super(Identifiable.INVALID_ID, HolderType.LEGAL_PERSON, "");
    }

    @Override
    public String getName() {
        return "";
    }

    @Override
    public int hashCode() {
        return (int) Identifiable.INVALID_ID;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (this == obj) {
            return true;
        }

        return (obj instanceof InvalidHolder);
    }

    private static class LazyHolder {
        private static final InvalidHolder INSTANCE = new InvalidHolder();
    }

    static InvalidHolder getInstance() {
        return LazyHolder.INSTANCE;
    }
}