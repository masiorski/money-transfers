package com.revolut.moneytransfers.models.holders;

import com.revolut.moneytransfers.enums.HolderType;
import com.revolut.moneytransfers.interfaces.Holder;
import lombok.Getter;

import java.util.Objects;

public abstract class AbstractHolder implements Holder {

    @Getter
    private final Long id;

    @Getter
    private final HolderType type;

    @Getter
    private final String taxNumber;

    AbstractHolder(Long id, HolderType type, String taxNumber) {
        Objects.requireNonNull(id, "Id cannot be null");
        Objects.requireNonNull(type, "PartyType cannot be null");
        Objects.requireNonNull(taxNumber, "Tax identification number cannot be null");

        this.id = id;
        this.type = type;
        this.taxNumber = taxNumber;
    }

    public final boolean isPrivatePerson() {
        return HolderType.PRIVATE_PERSON == type;
    }

    public final boolean isLegalPerson() {
        return HolderType.LEGAL_PERSON == type;
    }

    @Override
    public final String toString() {
        return String.format("Party{%s, type=%s, tax identification number=%s, id=%d}",
                getName(), this.getType(), getTaxNumber(), getId());
    }

    @Override
    public abstract int hashCode();

    @Override
    public abstract boolean equals(Object obj);

    public static Holder makeLegalPerson(Long id, String taxIdentificationNumber, String name) {
        return new DefaultLegalPerson(id, taxIdentificationNumber, name);
    }

    public static Holder makePrivatePerson(Long id, String taxIdentificationNumber, String firstName, String lastName) {
        return new DefaultPrivatePerson(id, taxIdentificationNumber, firstName, lastName);
    }

    public static Holder getInvalid() {
        return InvalidHolder.getInstance();
    }
}
