package com.revolut.moneytransfers.models.holders;

import com.revolut.moneytransfers.enums.HolderType;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.Objects;

abstract class LegalPerson extends AbstractHolder {

    private final String name;

    LegalPerson(Long id, String taxIdentificationNumber, String name) {
        super(id, HolderType.LEGAL_PERSON, taxIdentificationNumber);
        Objects.requireNonNull(name, "Name cannot be null");
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(39, 19)
                .append(name)
                .append(this.getType())
                .append(getTaxNumber())
                .toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (this == obj) {
            return true;
        }

        if (!(obj instanceof LegalPerson)) {
            return false;
        }

        LegalPerson other = (LegalPerson) obj;
        return new EqualsBuilder()
                .append(name, other.name)
                .append(this.getType(), other.getType())
                .append(getTaxNumber(), other.getTaxNumber())
                .isEquals();
    }
}
