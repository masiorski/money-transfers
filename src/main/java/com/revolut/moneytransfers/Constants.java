package com.revolut.moneytransfers;

public class Constants {

    private Constants() {}

    public static final long LOCK_WAIT_INTERVAL = 10L;
}
