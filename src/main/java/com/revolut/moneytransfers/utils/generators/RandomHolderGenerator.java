package com.revolut.moneytransfers.utils.generators;

import com.revolut.moneytransfers.interfaces.Holder;
import com.revolut.moneytransfers.interfaces.repositories.HolderRepository;
import com.revolut.moneytransfers.repositories.ApplicationContext;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

class RandomHolderGenerator extends AbstractGenerator {

    private final int holdersCount;

    RandomHolderGenerator(final ApplicationContext applicationContext, final int holdersCount) {
        super(applicationContext, "holders");
        this.holdersCount = holdersCount;
    }

    @Override
    List<Future<?>> doGenerate(final ExecutorService threadPool) {
        final List<Future<?>> futures = new ArrayList<>(holdersCount);
        for (int i = 0; i < holdersCount; ++i) {
            futures.add(threadPool.submit(this::generateParty));
        }
        return futures;
    }

    private void generateParty() {
        final HolderRepository holderRepository = applicationContext.getHolderRepository();
        final int idx = counter.incrementAndGet();
        if (idx % 2 == 0) {
            Holder pt = holderRepository.addLegalPerson(generateTaxId(idx, 10), generateCompanyName(idx));
            ids.add(pt.getId());
        } else {
            Holder pt = holderRepository.addPrivatePerson(generateTaxId(idx, 12), generateFirstName(idx), "Stark");
            ids.add(pt.getId());
        }
    }

    private static String generateCompanyName(final int idx) {
        if (idx % 4 == 0) {
            return "Stark" + idx;
        } else {
            if (idx % 6 == 0) {
                return "Lannister" + idx;
            }
        }
        return "Targaryen" + idx;
    }

    private static String generateFirstName(final int idx) {
        if (idx % 3 == 0) {
            return "Eddard" + idx;
        } else {
            if (idx % 5 == 0) {
                return "Bran" + idx;
            } else {
                if (idx % 7 == 0) {
                    return "Sansa" + idx;
                }
            }
        }
        return "Robb" + idx;
    }

    private static String generateTaxId(final int idx, final int length) {
        return StringUtils.leftPad(String.valueOf(idx), length, '0');
    }
}
