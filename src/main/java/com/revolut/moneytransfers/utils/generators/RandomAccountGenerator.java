package com.revolut.moneytransfers.utils.generators;

import com.revolut.moneytransfers.interfaces.Account;
import com.revolut.moneytransfers.interfaces.Holder;
import com.revolut.moneytransfers.interfaces.repositories.AccountRepository;
import com.revolut.moneytransfers.repositories.ApplicationContext;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

class RandomAccountGenerator extends AbstractGenerator {

    private final int accountsPerClient;
    private final List<Long> partyIds;

    RandomAccountGenerator(final ApplicationContext applicationContext, final List<Long> partyIds, final int accountsPerClient) {
        super(applicationContext, "accounts");
        Objects.requireNonNull(partyIds, "Ids list cannot be null");
        this.partyIds = partyIds;
        this.accountsPerClient = accountsPerClient;
    }

    @Override
    List<Future<?>> doGenerate(final ExecutorService threadPool) {
        final List<Future<?>> futures = new ArrayList<>(partyIds.size() * accountsPerClient);
        for (Long partyId : partyIds) {
            Runnable runnableTask = () -> this.generateAccount(partyId);
            futures.add(threadPool.submit(runnableTask));
        }
        return futures;
    }

    private void generateAccount(Long partyId) {
        final AccountRepository accountRepository = applicationContext.getAccountRepository();
        final Holder pt = applicationContext.getHolderRepository().getById(partyId);
        if (pt.isValid()) {
            for (int i = 0; i < accountsPerClient; ++i) {
                final int idx = counter.incrementAndGet();
                final Account a = accountRepository.addPassiveAccount(generateNumber(idx), pt);
                ids.add(a.getId());
            }
        } else {
             LOGGER.error("Party with id = {} not found", partyId);
        }
    }

    private static String generateNumber(final int idx) {
        return "4080281010" + StringUtils.leftPad(String.valueOf(idx), 10, '0');
    }
}
