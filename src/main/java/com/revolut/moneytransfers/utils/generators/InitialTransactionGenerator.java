package com.revolut.moneytransfers.utils.generators;

import com.revolut.moneytransfers.interfaces.Account;
import com.revolut.moneytransfers.interfaces.Transaction;
import com.revolut.moneytransfers.interfaces.repositories.AccountRepository;
import com.revolut.moneytransfers.repositories.ApplicationContext;
import com.revolut.moneytransfers.utils.TransactionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

class InitialTransactionGenerator extends AbstractGenerator {

    private final List<Long> accountIds;
    private final boolean runImmediately;

    InitialTransactionGenerator(ApplicationContext applicationContext, List<Long> accountIds, boolean runImmediately) {
        super(applicationContext, "initial transactions");
        Objects.requireNonNull(accountIds, "Ids list cannot be null");
        this.accountIds = accountIds;
        this.runImmediately = runImmediately;
    }

    @Override
    List<Future<?>> doGenerate(final ExecutorService threadPool) {
        final List<Future<?>> futures = new ArrayList<>(accountIds.size());
        for (Long accountId : accountIds) {
            Runnable runnableTask = () -> this.generateInitialTransaction(accountId);
            futures.add(threadPool.submit(runnableTask));
        }
        return futures;
    }

    private void generateInitialTransaction(final Long creditAccountId) {
        final AccountRepository accountRepository = applicationContext.getAccountRepository();
        final Account debit = accountRepository.getOurBankMainAccount();
        final Account credit = accountRepository.getById(creditAccountId);
        if (credit.isValid()) {
            final BigDecimal amount = TransactionUtils.generateAmount(500_000, 1000_000);
            final Transaction transaction = applicationContext.getTransactionRepository().add(debit, credit, amount);
            if (runImmediately) {
                transaction.run();
            }
            ids.add(transaction.getId());
        } else {
             LOGGER.error("Credit account with id = {} not found", creditAccountId);
        }
    }
}
