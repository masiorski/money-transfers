package com.revolut.moneytransfers.utils.generators;

import com.revolut.moneytransfers.repositories.ApplicationContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class DataGenerator {

    private static final  Logger LOGGER = LoggerFactory.getLogger(DataGenerator.class);

    private final ApplicationContext applicationContext;
    private int holdersCount = 50_000;
    private int accountsPerClient = 4;
    private boolean initialTransactions = true;
    private boolean runImmediately = true;
    private int clientTransactionsCount = 1_000;

    private DataGenerator(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    public DataGenerator withHoldersCount(final int count) {
        holdersCount = count;
        return this;
    }

    public DataGenerator withAccountsPerClient(final int count) {
        this.accountsPerClient = count;
        return this;
    }

    public DataGenerator withoutInitialTransactions() {
        this.initialTransactions = false;
        return this;
    }

    public DataGenerator withoutRunningTransactions() {
        this.runImmediately = false;
        return this;
    }

    public DataGenerator withClientTransactions(final int count) {
        this.clientTransactionsCount = count;
        if (count > 0) {
            initialTransactions = true;
        }
        return this;
    }

    public DataGenerator withoutClientTransactions() {
        this.clientTransactionsCount = 0;
        return this;
    }

    public void generate() {
        try {
            final List<Long> holdersIds = generateHolders();
            final List<Long> accountIds = generateAccounts(holdersIds);
            if (initialTransactions) {
                generateInitialTransactions(accountIds);
                if (clientTransactionsCount > 0) {
                    generateClientTransactions(accountIds);
                }
            }
        } catch (Exception e) {
             LOGGER.error(e.getLocalizedMessage(), e);
        }
    }

    private List<Long> generateHolders() {
        final AbstractGenerator partyGenerator = new RandomHolderGenerator(applicationContext, holdersCount);
        final List<Long> partyIds = partyGenerator.generate();
        // Our bank already exists
        LOGGER.debug("Party ids count = {}", partyIds.size());
        LOGGER.debug("Party repository size = {}", applicationContext.getHolderRepository().size());
        return partyIds;
    }

    private List<Long> generateAccounts(final List<Long> partyIds) {
        final AbstractGenerator accountGenerator = new RandomAccountGenerator(applicationContext, partyIds, accountsPerClient);
        final List<Long> accountIds = accountGenerator.generate();
        // Our bank account already exists
        LOGGER.debug("Account ids count = {}", accountIds.size());
        LOGGER.debug("Account repository size = {}", applicationContext.getAccountRepository().size());
        return accountIds;
    }

    private void generateInitialTransactions(final List<Long> accountIds) {
        final AbstractGenerator initialTransactionGenerator = new InitialTransactionGenerator(applicationContext, accountIds, runImmediately);
        final List<Long> initialTrnIds = initialTransactionGenerator.generate();
        LOGGER.debug("Initial transaction ids count = {}", initialTrnIds.size());
        LOGGER.debug("Transaction repository size = {}", applicationContext.getTransactionRepository().size());
        applicationContext.getAccountRepository().validateBalance();
    }

    private void generateClientTransactions(final List<Long> accountIds) {
        final AbstractGenerator transactionGenerator = new RandomTransactionGenerator(
                applicationContext, accountIds, runImmediately, 10, clientTransactionsCount);
        final List<Long> trnIds = transactionGenerator.generate();
        LOGGER.debug("Transaction ids count = {}", trnIds.size());
        LOGGER.debug("Transaction repository size = {}", applicationContext.getTransactionRepository().size());
        applicationContext.getAccountRepository().validateBalance();
    }

    public static DataGenerator getInstance(ApplicationContext applicationContext) {
        return new DataGenerator(applicationContext);
    }
}
