package com.revolut.moneytransfers.utils.generators;

import com.revolut.moneytransfers.interfaces.Account;
import com.revolut.moneytransfers.interfaces.Transaction;
import com.revolut.moneytransfers.interfaces.repositories.AccountRepository;
import com.revolut.moneytransfers.repositories.ApplicationContext;
import com.revolut.moneytransfers.utils.TransactionUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

class RandomTransactionGenerator extends AbstractGenerator {

    private final int trnCount;
    private final List<Long> accountIds;
    private final boolean runImmediately;

    RandomTransactionGenerator(final ApplicationContext applicationContext, final List<Long> accountIds,
                               final boolean runImmediately, final int threadPoolSize, int trnCount) {
        super(applicationContext, "clients transactions", threadPoolSize);
        Objects.requireNonNull(accountIds, "Ids list cannot be null");
        this.accountIds = accountIds;
        this.runImmediately = runImmediately;
        this.trnCount = trnCount;
    }

    @Override
    List<Future<?>> doGenerate(final ExecutorService threadPool) {
        final List<Future<?>> futures = new ArrayList<>(trnCount);
        for (int i = 0; i < trnCount; ++i) {
            futures.add(threadPool.submit(this::generateTransaction));
        }
        return futures;
    }

    private void generateTransaction() {
        final AccountRepository accountRepository = applicationContext.getAccountRepository();
        final Pair<Long, Long> randomIds = TransactionUtils.getRandomAccountIds(accountIds);
        final Account debit = accountRepository.getById(randomIds.getLeft());
        if (debit.isValid()) {
            final Account credit = accountRepository.getById(randomIds.getRight());
            if (credit.isValid()) {
                final BigDecimal amount = TransactionUtils.generateAmount(5_000, 100_000);
                final Transaction transaction = applicationContext.getTransactionRepository().add(debit, credit, amount);
                if (runImmediately) {
                    transaction.run();
                }
                ids.add(transaction.getId());
            } else {
                 LOGGER.error("Credit account with id = {} not found", randomIds.getRight());
            }
        } else {
             LOGGER.error("Debit account with id = {} not found", randomIds.getLeft());
        }
    }
}
