package com.revolut.moneytransfers.utils;

import com.revolut.moneytransfers.interfaces.Account;
import com.revolut.moneytransfers.interfaces.Transaction;
import com.revolut.moneytransfers.interfaces.repositories.Repository;
import com.revolut.moneytransfers.repositories.ApplicationContext;
import com.revolut.moneytransfers.utils.generators.DataGenerator;
import lombok.Getter;

import java.util.NoSuchElementException;
import java.util.Objects;

public final class Bank {

    @Getter
    private final ApplicationContext applicationContext;

    private Bank(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    public void generateData() {
        DataGenerator.getInstance(applicationContext)
                .withHoldersCount(100)
                .withAccountsPerClient(2)
                .withClientTransactions(10_000)
                .generate();
    }

    public Transaction transfer(TransactionPayload payload) {
        Objects.requireNonNull(payload, "Transaction data cannot be null");

        final Repository<Account> accountRepository = applicationContext.getAccountRepository();
        final Account from = accountRepository.getById(payload.getFromAccountId());
        validateAccount(from, payload.getFromAccountId());
        final Account to = accountRepository.getById(payload.getToAccountId());
        validateAccount(to, payload.getToAccountId());

        final Transaction transaction = applicationContext.getTransactionRepository().add(from, to, payload.getAmount());
        transaction.run();
        return transaction;
    }

    private void validateAccount(Account account, Long id) {
        if (account.isNotValid()) {
            throw new NoSuchElementException(String.format("Account with id %d is not found", id));
        }
    }

    private static class LazyHolder {
        static final Bank INSTANCE = new Bank(ApplicationContext.initialize());
    }

    public static Bank getInstance() {
        return LazyHolder.INSTANCE;
    }
}
