package com.revolut.moneytransfers.utils;

import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public final class TransactionUtils {

    private static final  Logger LOGGER = LoggerFactory.getLogger(TransactionUtils.class);

    private TransactionUtils() {}

    public static Pair<Long, Long> getRandomAccountIds(final List<Long> accountIds) {
        final int fromIdx = getRandom().nextInt(accountIds.size());
        final int toIdx = fromIdx != 0 ? fromIdx - 1 : fromIdx + 1;
        final Pair<Long, Long> result = Pair.of(accountIds.get(fromIdx), accountIds.get(toIdx));
         LOGGER.trace("Generated accounts pair = {}", result);
        return result;
    }

    private static Random getRandom() {
        return ThreadLocalRandom.current();
    }

    public static BigDecimal generateAmount(int min, int max) {
        final BigDecimal amount = BigDecimal.valueOf(min + getRandom().nextInt(max - min), 2);
         LOGGER.trace("Generated amount = {}", amount);
        return amount;
    }
}
