package com.revolut.moneytransfers.utils;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.math.BigDecimal;

@Getter
@AllArgsConstructor
public final class TransactionPayload {

    private final Long fromAccountId;
    private final Long toAccountId;
    private final BigDecimal amount;
}
