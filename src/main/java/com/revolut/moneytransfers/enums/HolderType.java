package com.revolut.moneytransfers.enums;

public enum HolderType {
    PRIVATE_PERSON,
    LEGAL_PERSON
}
