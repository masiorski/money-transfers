package com.revolut.moneytransfers.enums;

public enum TransactionState {
    NEW,
    INSUFFICIENT_FUNDS,
    COMPLETED,
    CONCURRENCY_ERROR,
    RESTARTED
}
