package com.revolut.moneytransfers.repositories;

import com.revolut.moneytransfers.interfaces.repositories.AccountRepository;
import com.revolut.moneytransfers.interfaces.repositories.HolderRepository;
import com.revolut.moneytransfers.interfaces.repositories.TransactionRepository;
import lombok.Getter;

@Getter
public class ApplicationContext {

    private final HolderRepository holderRepository;
    private final AccountRepository accountRepository;
    private final TransactionRepository transactionRepository;

    private ApplicationContext(HolderRepository holderRepository,
                               AccountRepository accountRepository,
                               TransactionRepository transactionRepository) {
        this.holderRepository = holderRepository;
        this.accountRepository = accountRepository;
        this.transactionRepository = transactionRepository;
    }

    public static ApplicationContext initialize() {
        final HolderRepository holderRepository = new DefaultHolderRepository();
        final AccountRepository accountRepository = new DefaultAccountRepository(holderRepository);
        final TransactionRepository transactionRepository = new DefaultTransactionRepository();
        return new ApplicationContext(holderRepository, accountRepository, transactionRepository);
    }
}
