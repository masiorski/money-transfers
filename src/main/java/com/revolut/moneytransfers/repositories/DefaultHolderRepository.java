package com.revolut.moneytransfers.repositories;

import com.revolut.moneytransfers.interfaces.Holder;
import com.revolut.moneytransfers.interfaces.repositories.PagedResult;
import com.revolut.moneytransfers.interfaces.repositories.HolderRepository;
import com.revolut.moneytransfers.models.holders.AbstractHolder;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicLong;

final class DefaultHolderRepository implements HolderRepository {

    private final AtomicLong counter = new AtomicLong(0L);
    private final ConcurrentMap<Long, Holder> holders = new ConcurrentHashMap<>();
    private final Long ourBankId;

    DefaultHolderRepository() {
        final Holder ourBank = addLegalPerson("7703408188", "Revolut LLC");
        ourBankId = ourBank.getId();
    }

    @Override
    public Holder addLegalPerson(String taxIdentificationNumber, String name) {
        final Holder legalPerson = AbstractHolder.makeLegalPerson(counter.incrementAndGet(), taxIdentificationNumber, name);
        holders.putIfAbsent(legalPerson.getId(), legalPerson);
        return legalPerson;
    }

    @Override
    public Holder addPrivatePerson(String taxIdentificationNumber, String firstName, String lastName) {
        final Holder privatePerson = AbstractHolder.makePrivatePerson(counter.incrementAndGet(), taxIdentificationNumber, firstName, lastName);
        holders.putIfAbsent(privatePerson.getId(), privatePerson);
        return privatePerson;
    }

    @Override
    public Holder getById(Long id) {
        return holders.getOrDefault(id, getInvalid());
    }

    @Override
    public Holder getOurBank() {
        return getById(ourBankId);
    }

    @Override
    public PagedResult<Holder> getAll(int pageNumber, int recordsPerPage) {
        return PagedResultImpl.from(pageNumber, recordsPerPage, holders);
    }

    @Override
    public Holder getInvalid() {
        return AbstractHolder.getInvalid();
    }

    @Override
    public int size() {
        return holders.size();
    }
}
