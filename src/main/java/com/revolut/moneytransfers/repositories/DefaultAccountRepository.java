package com.revolut.moneytransfers.repositories;

import com.revolut.moneytransfers.exceptions.InvalidBalanceException;
import com.revolut.moneytransfers.interfaces.Account;
import com.revolut.moneytransfers.interfaces.Currency;
import com.revolut.moneytransfers.interfaces.Holder;
import com.revolut.moneytransfers.interfaces.repositories.AccountRepository;
import com.revolut.moneytransfers.interfaces.repositories.PagedResult;
import com.revolut.moneytransfers.interfaces.repositories.HolderRepository;
import com.revolut.moneytransfers.models.accounts.AbstractAccount;
import com.revolut.moneytransfers.models.currencies.BaseCurrency;
import com.revolut.moneytransfers.utils.validators.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

final class DefaultAccountRepository implements AccountRepository {

    private static final  Logger LOGGER = LoggerFactory.getLogger(DefaultAccountRepository.class);
    private static final BigDecimal INITIAL_BALANCE = BigDecimal.valueOf(100_000_000.00d);

    private final AtomicLong counter;
    private final ConcurrentMap<Long, Account> accounts;
    private final HolderRepository holderRepository;
    private final Long ourBankAccountId;

    DefaultAccountRepository(HolderRepository holderRepository) {
        this.holderRepository = holderRepository;
        this.counter = new AtomicLong(0L);
        this.accounts = new ConcurrentHashMap<>();
        final Account ourBankAccount = addOurBankAccount("20202810100000010001", INITIAL_BALANCE);
        this.ourBankAccountId = ourBankAccount.getId();
    }

    @Override
    public Account getById(Long id) {
        return accounts.getOrDefault(id, getInvalid());
    }

    @Override
    public Account getInvalid() {
        return AbstractAccount.getInvalid();
    }

    @Override
    public Account addOurBankAccount(String number, BigDecimal balance) {
        return addOurBankAccount(BaseCurrency.getDefault(), number, balance);
    }

    @Override
    public Account addOurBankAccount(Currency currency, String number, BigDecimal balance) {
        final Account account = AbstractAccount.makeActiveAccount(
                counter.incrementAndGet(), currency, number, holderRepository.getOurBank(), balance);
        accounts.putIfAbsent(account.getId(), account);
        return account;
    }

    @Override
    public BigDecimal getInitialBalance() {
        return INITIAL_BALANCE;
    }

    @Override
    public Account getOurBankMainAccount() {
        return getById(ourBankAccountId);
    }

    @Override
    public Account addPassiveAccount(Currency currency, String number, Holder holder) {
        final Account account = AbstractAccount.makePassiveAccount(counter.incrementAndGet(), currency, number, holder);
        accounts.putIfAbsent(account.getId(), account);
        return account;
    }

    @Override
    public Account addPassiveAccount(String number, Holder holder) {
        return addPassiveAccount(BaseCurrency.getDefault(), number, holder);
    }

    @Override
    public int size() {
        return accounts.size();
    }

    @Override
    public void validateBalance() {
        final long timeStart = System.nanoTime();
        try {
            final BigDecimal expected = getInitialBalance();
            BigDecimal totalSum = BigDecimal.ZERO;
            for (Account a : accounts.values()) {
                Validator.validateAmountNotNegative(a);
                totalSum = totalSum.add(a.getBalance());
            }
            if (totalSum.compareTo(expected) != 0) {
                throw new InvalidBalanceException(expected, totalSum);
            }
             LOGGER.debug("Balance is valid! {} == {}", expected, totalSum);
        } finally {
            final long timeEnd = System.nanoTime();
             LOGGER.info("Balance validation is completed. Time elapsed = {} microseconds", (timeEnd - timeStart) / 1_000);
        }
    }

    @Override
    public PagedResult<Account> getAll(int pageNumber, int recordsPerPage) {
        return PagedResultImpl.from(pageNumber, recordsPerPage, accounts);
    }

    @Override
    public Collection<Account> getByHolder(Holder holder) {
        return Collections.unmodifiableCollection(
                accounts.values().stream()
                .filter(a -> a.getHolder().equals(holder))
                .sorted(Comparator.comparing(Account::getId))
                .collect(Collectors.toList()));
    }
}
