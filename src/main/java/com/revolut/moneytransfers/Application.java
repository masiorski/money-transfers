package com.revolut.moneytransfers;

import com.revolut.moneytransfers.interfaces.Account;
import com.revolut.moneytransfers.interfaces.Identifiable;
import com.revolut.moneytransfers.interfaces.Holder;
import com.revolut.moneytransfers.interfaces.Transaction;
import com.revolut.moneytransfers.interfaces.repositories.AccountRepository;
import com.revolut.moneytransfers.interfaces.repositories.Repository;
import com.revolut.moneytransfers.interfaces.repositories.TransactionRepository;
import com.revolut.moneytransfers.utils.Bank;
import com.revolut.moneytransfers.utils.JsonUtils;
import com.revolut.moneytransfers.utils.PaginationParams;
import com.revolut.moneytransfers.utils.TransactionPayload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;
import spark.Spark;

import javax.servlet.http.HttpServletResponse;
import java.util.NoSuchElementException;
import java.util.Objects;

final class Application {

    private static final  Logger LOGGER = LoggerFactory.getLogger(Application.class);
    private static final String GENERATE_DATA = "generate_data";

    static void start() {
        Application.main(null);
        Spark.awaitInitialization();
    }

    static void startWithData() {
        Application.main(new String[] { GENERATE_DATA });
        Spark.awaitInitialization();
    }

    static void stop() {
        Spark.stop();
        Spark.awaitStop();
    }

    public static void main(String[] args) {
        final boolean generate = Objects.nonNull(args) && args.length > 0 && GENERATE_DATA.equals(args[0].toLowerCase());
        generateData(generate);
        Spark.port(8080);
        Spark.threadPool(64);
        Spark.after((req, res) -> res.type("application/json"));
        initializeHoldersRoutes();
        initializeAccountRoutes();
        initializeTransactionRoutes();
        initializeExceptionsHandling();
    }

    private static void initializeHoldersRoutes() {
        Spark.get("/holders", (req, res) -> {
            final Repository<Holder> repository = Bank.getInstance().getApplicationContext().getHolderRepository();
            final PaginationParams params = PaginationParams.from(req);
            return JsonUtils.make().toJson(repository.getAll(params));
        });

        Spark.get("/holder/:id", (req, res) -> {
            final Repository<Holder> repository = Bank.getInstance().getApplicationContext().getHolderRepository();
            return JsonUtils.make().toJson(findById(Holder.class, repository, req));
        });

        Spark.get("/holder/:id/accounts", (req, res) -> {
            final Repository<Holder> repository = Bank.getInstance().getApplicationContext().getHolderRepository();
            final Holder holder = findById(Holder.class, repository, req);
            final AccountRepository accountRepository = Bank.getInstance().getApplicationContext().getAccountRepository();
            return JsonUtils.make().toJson(accountRepository.getByHolder(holder));
        });
    }

    private static void initializeAccountRoutes() {
        Spark.get("/accounts", (req, res) -> {
            final Repository<Account> repository = Bank.getInstance().getApplicationContext().getAccountRepository();
            final PaginationParams params = PaginationParams.from(req);
            return JsonUtils.make().toJson(repository.getAll(params));
        });

        Spark.get("/account/:id", (req, res) -> {
            final Repository<Account> repository = Bank.getInstance().getApplicationContext().getAccountRepository();
            return JsonUtils.make().toJson(findById(Account.class, repository, req));
        });

        Spark.get("/account/:id/transactions", (req, res) -> {
            final Repository<Account> repository = Bank.getInstance().getApplicationContext().getAccountRepository();
            final Account account = findById(Account.class, repository, req);
            final TransactionRepository transactionRepository = Bank.getInstance().getApplicationContext().getTransactionRepository();
            final PaginationParams params = PaginationParams.from(req);
            return JsonUtils.make().toJson(transactionRepository.getByAccount(account, params));
        });
    }

    private static void initializeTransactionRoutes() {
        Spark.get("/transactions", (req, res) -> {
            final Repository<Transaction> repository = Bank.getInstance().getApplicationContext().getTransactionRepository();
            final PaginationParams params = PaginationParams.from(req);
            return JsonUtils.make().toJson(repository.getAll(params));
        });

        Spark.get("/transaction/:id", (req, res) -> {
            final Repository<Transaction> repository = Bank.getInstance().getApplicationContext().getTransactionRepository();
            return JsonUtils.make().toJson(findById(Transaction.class, repository, req));
        });

        Spark.post("/transaction", (req, res) -> {
            final TransactionPayload payload = JsonUtils.make().fromJson(req.body(), TransactionPayload.class);
            final Transaction transaction = Bank.getInstance().transfer(payload);
            res.status(HttpServletResponse.SC_CREATED);
            res.header("Location", "/transaction/" + transaction.getId());
            return JsonUtils.make().toJson(transaction);
        });
    }

    private static void initializeExceptionsHandling() {
        Spark.exception(IllegalArgumentException.class, (e, req, res) -> fillErrorInfo(res, e, HttpServletResponse.SC_BAD_REQUEST));
        Spark.exception(NullPointerException.class, (e, req, res) -> fillErrorInfo(res, e, HttpServletResponse.SC_BAD_REQUEST));
        Spark.exception(NumberFormatException.class, (e, req, res) -> fillErrorInfo(res, e, HttpServletResponse.SC_BAD_REQUEST));
        Spark.exception(NoSuchElementException.class, (e, req, res) -> fillErrorInfo(res, e, HttpServletResponse.SC_NOT_FOUND));
    }

    private static void generateData(final boolean generate) {
        if (generate) {
            try {
                Bank.getInstance().generateData();
            } catch (Exception e) {
                 LOGGER.error(e.getLocalizedMessage(), e);
            }
        }
    }

    private static void fillErrorInfo(Response res, Exception err, int errCode) {
        res.type("application/json");
        res.status(errCode);
        res.body(JsonUtils.toJson(err, errCode));
    }

    private static <T extends Identifiable> T findById(Class<T> type, Repository<T> repository, Request req) {
        final Long id = getId(req);
        final T t = repository.getById(id);
        if (t.isNotValid()) {
            throw new NoSuchElementException(String.format("%s with id %d is not found", type.getSimpleName(), id));
        }
        return t;
    }

    private static Long getId(Request req) {
        return Long.valueOf(req.params("id"), 10);
    }
}
