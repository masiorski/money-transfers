# money-transfers

### How to run:
`bash build_and_run.sh`

### REST API:
Bank account holders:
* GET http://localhost:8080/holders?limit=10
* GET http://localhost:8080/holder/:id
* GET http://localhost:8080/holder/:id/accounts

Bank accounts:
* GET http://localhost:8080/accounts?limit=10
* GET http://localhost:8080/account/:id
* GET http://localhost:8080/account/:id/transactions?limit=100

Transactions:
* GET http://localhost:8080/transactions?limit=100
* GET http://localhost:8080/transaction/:id
* POST http://localhost:8080/transaction

### Tests:
Integration:
* com.revolut.moneytransfers.ApplicationTest
* com.revolut.moneytransfers.TransferMoneyTest

JUnits:
* com.revolut.moneytransfers.models.*

DAO:
* com.revolut.moneytransfers.repositories.*